<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Beli Tiket</name>
   <tag></tag>
   <elementGuidId>0ce6e2ae-f6ce-444d-a51c-c4a1fe69020e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-button']/form/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-lg</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-primary btn-lg' and (text() = 'Beli Tiket' or . = 'Beli Tiket')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7d31a429-84a8-4c56-82ab-3fee0d742b77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg</value>
      <webElementGuid>cc06a233-dd5f-4769-970b-560dbff398aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Beli Tiket</value>
      <webElementGuid>f58ef80b-dba7-4a1c-bfd1-e9f3adffc895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-button&quot;)/form[1]/button[@class=&quot;btn btn-primary btn-lg&quot;]</value>
      <webElementGuid>7913636f-0cfc-440d-8938-397817fe414a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-button']/form/button</value>
      <webElementGuid>d8ec99b2-1a28-4144-b70b-3a585cbe6d91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::button[1]</value>
      <webElementGuid>18d34c4e-7a78-4cdf-a303-8a9bb8ae2fd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::button[1]</value>
      <webElementGuid>fc3434c2-da5b-4993-9ecd-a1e180e37682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::button[1]</value>
      <webElementGuid>eb2ef8f7-eea1-4416-948b-921a3fbc673d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[1]/preceding::button[1]</value>
      <webElementGuid>e3b5b1b5-3df4-45ea-a101-6b7c9f3d256b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Beli Tiket']/parent::*</value>
      <webElementGuid>62a0fe33-8e57-4e58-a943-5ef3d3fcd417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[6]/form/button</value>
      <webElementGuid>14150720-2182-4a61-882d-6dcc79231569</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Beli Tiket' or . = 'Beli Tiket')]</value>
      <webElementGuid>9ea29fbe-26da-4e8f-a395-9b8a13ce96a4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

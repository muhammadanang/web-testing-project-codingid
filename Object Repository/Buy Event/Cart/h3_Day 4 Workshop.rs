<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Day 4 Workshop</name>
   <tag></tag>
   <elementGuidId>7805f683-9619-4974-b28d-db270c392053</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-7 > div > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>f9f77662-e601-4bca-a876-debc8d76a915</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Day 4: Workshop</value>
      <webElementGuid>c5daffc4-7324-41eb-9be7-0b007e963b8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[1]/h3[1]</value>
      <webElementGuid>6a73cf41-c98d-42e5-98b3-eb647f4ebf63</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      <webElementGuid>1347ce3f-dc21-4c64-9a5a-84d063b69b92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pembelian Saya'])[1]/following::h3[1]</value>
      <webElementGuid>bd722fa8-8d81-4797-9a8f-251afe14d00b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/preceding::h3[1]</value>
      <webElementGuid>f7442f30-a9ad-42c7-954c-7a27950131cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 4: Workshop']/parent::*</value>
      <webElementGuid>86593fb0-1db4-4c7d-b402-e07d11630039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>52d67548-7a1f-4da6-a7c4-6859289ebc33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = '
                                                    Day 4: Workshop' or . = '
                                                    Day 4: Workshop')]</value>
      <webElementGuid>7f4cd729-c45e-4970-ac01-071a1cdefb5d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

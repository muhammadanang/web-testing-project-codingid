<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Predict Machine Learning</name>
   <tag></tag>
   <elementGuidId>ea470c4d-2010-4303-89bf-3f4343fb92f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-7 > div > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>e8eaa524-5a29-4a39-8c42-b5709508c53e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Day 3: Predict using Machine Learning</value>
      <webElementGuid>7d62ebd4-7c37-44e0-a283-bbfbb17a0fe0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[1]/h3[1]</value>
      <webElementGuid>3dfaed11-e04d-4c7f-985b-223fc90e9dc2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div[2]/div/h3</value>
      <webElementGuid>42bff1c5-ac5f-4006-a026-45ed6477916c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pembelian Saya'])[1]/following::h3[1]</value>
      <webElementGuid>e44ac5fe-8c85-493d-9c93-0396de16de17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/preceding::h3[1]</value>
      <webElementGuid>c063150d-b3c9-44c4-9d71-84bfd4360fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 3: Predict using Machine Learning']/parent::*</value>
      <webElementGuid>a53ce0a8-9d37-456f-95d9-7537b9f25226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>f0e9d006-f9cf-4bef-9c9d-a3e1d61f01b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = '
                                                    Day 3: Predict using Machine Learning' or . = '
                                                    Day 3: Predict using Machine Learning')]</value>
      <webElementGuid>85b444e3-a343-4b01-b956-f51a498d370d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Day 3 Open</name>
   <tag></tag>
   <elementGuidId>e80f1344-1b7c-4271-b980-28864072758c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='blockListEvent']/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.cardOuter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3663ee34-e5c8-4a6c-adb3-5a9b58ec01b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardOuter</value>
      <webElementGuid>19819044-0a2a-4e36-b8c7-15073e1afc34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    </value>
      <webElementGuid>71161f67-38ef-457d-8eb7-3def5e5fb3fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]</value>
      <webElementGuid>4d5a98dd-d7a0-48f1-8ad9-7caaa8c624f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='blockListEvent']/a/div</value>
      <webElementGuid>6bb521f3-f1d2-42a6-925b-db136feb9f3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/following::div[5]</value>
      <webElementGuid>4d307de7-5401-412e-b055-3d8d86d74169</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::div[10]</value>
      <webElementGuid>797dc1a4-5f8f-4252-bb54-333b8a1dffdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>065698f6-a473-4e3b-8ad3-94d5349a3192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ' or . = '
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    ')]</value>
      <webElementGuid>2194cda7-af37-4b51-8cf5-bdbfd028115d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Detail Pembayaran</name>
   <tag></tag>
   <elementGuidId>73761fd9-985f-42b2-9e93-687b777b3eb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='modalform']/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modalform > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>c5012438-0daa-4f6a-917f-0dcd2502ac8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Detail Pembayaran </value>
      <webElementGuid>e7e0bd4e-6208-4cee-ae65-cd9291834932</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalform&quot;)/h3[1]</value>
      <webElementGuid>b23422e6-cb71-4c2c-ad97-98ec43e9cb8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/h3</value>
      <webElementGuid>833c35dd-d1ce-46c8-a5cb-33fc9889ed5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/following::h3[1]</value>
      <webElementGuid>885254d6-a36b-46a5-92eb-0e8ffc6bd06d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/following::h3[1]</value>
      <webElementGuid>46900805-136d-48f1-b17c-923b127c2152</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Price'])[1]/preceding::h3[1]</value>
      <webElementGuid>3ab171d2-e43c-49dc-a376-4df19fa649b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discount Referral / Voucher'])[1]/preceding::h3[1]</value>
      <webElementGuid>907ad33e-f0c3-4c19-a4e6-d53f9229c8d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Detail Pembayaran']/parent::*</value>
      <webElementGuid>797c117f-b033-45ad-9fe8-a719c02715ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/h3</value>
      <webElementGuid>45bc8e83-eaf0-4075-b071-daae1caa4c11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Detail Pembayaran ' or . = 'Detail Pembayaran ')]</value>
      <webElementGuid>3bd2cff0-8f8a-4d0e-88eb-6e50f6f7d50e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

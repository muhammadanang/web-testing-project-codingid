<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_payment_method</name>
   <tag></tag>
   <elementGuidId>c261e5bd-224c-4984-8bc0-da4a6278a0f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bank']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#bank</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0cb0877b-6ddb-4939-bec6-da442b88cbe7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>14d43b16-b1b6-4552-bded-ff8331aaafcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-inline payment</value>
      <webElementGuid>d1c0c5b3-a052-4d72-b173-300667cdcba9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>payment_method</value>
      <webElementGuid>8a13f104-8799-4c6b-94cd-d57099b0360d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>e-wallet</value>
      <webElementGuid>5da39ee0-d8f3-4ddb-aec6-0fcdf91660df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bank</value>
      <webElementGuid>03edd8b3-dbf4-412f-bf0b-a9088628410c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)</value>
      <webElementGuid>90cce0f5-0df2-456b-a9f8-883c40c8adf4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bank']</value>
      <webElementGuid>fba547cb-4ed0-4061-8c50-5854c3cda7d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td/input</value>
      <webElementGuid>bda41770-286b-4e94-a9f8-4057ddaf5f7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/input</value>
      <webElementGuid>c7e947e7-aaf7-48ef-9190-799e5b9e985e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'payment_method' and @id = 'bank']</value>
      <webElementGuid>377c424e-d4f6-407d-82da-d23623bbfcbe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

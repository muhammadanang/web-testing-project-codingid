<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Coding.ID Privacy and Policy</name>
   <tag></tag>
   <elementGuidId>c4b28b9a-bac7-44c5-b8ac-7942c1905a8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID Service'])[1]/preceding::b[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
      <webElementGuid>a94fc8ca-09a9-48ec-b0a8-e60c684769a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding.ID Privacy and Policy</value>
      <webElementGuid>2d079b11-34e8-431f-9cb8-0e4f0e2b2ea3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll no-mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[4]/div[1]/h1[1]/b[1]</value>
      <webElementGuid>6ed5cae6-8c21-411c-93f0-c0faadce4985</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID Service'])[1]/preceding::b[1]</value>
      <webElementGuid>00fe9eb5-96e1-44d4-b887-fd39c34a7b67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi yang Kami Kumpulkan'])[1]/preceding::b[2]</value>
      <webElementGuid>dfe265b3-c7c2-45a0-bfb7-525564fd9ad7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding.ID Privacy and Policy']/parent::*</value>
      <webElementGuid>a014b889-99f6-4182-a0ae-1ed9c58cf520</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//b</value>
      <webElementGuid>f3900580-0ca0-437f-9c23-64d0351a4304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//b[(text() = 'Coding.ID Privacy and Policy' or . = 'Coding.ID Privacy and Policy')]</value>
      <webElementGuid>cb62aaff-2bee-4905-b11a-9dec7e478c1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_CodingID Service</name>
   <tag></tag>
   <elementGuidId>d57f1e63-8385-4081-8a79-197caf88e30c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID Privacy and Policy'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ol > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>049d9327-220f-485e-84b9-0802453139ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding.ID Service 
                    Selamat datang di Coding.ID app, One Stop Learning App yang memberikan layanan berupa konten –
                    konten belajar programming dalam bentuk kursus online, event dan lain sebagainya.
                </value>
      <webElementGuid>24c06b00-0135-426a-ae04-b8e9bd31441b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll no-mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[4]/div[1]/div[1]/ol[1]/li[1]</value>
      <webElementGuid>b4b7601e-024e-403b-b896-ff7e4c526b8e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID Privacy and Policy'])[1]/following::li[1]</value>
      <webElementGuid>dd4fc2b4-fddf-4e8e-be0f-8e22afbb14c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi yang Kami Kumpulkan'])[1]/preceding::li[1]</value>
      <webElementGuid>154cb7d3-fc90-454e-bf6d-7762561f4757</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ol/li</value>
      <webElementGuid>60dfe23a-5ed9-4be7-a37a-a937ae803578</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Coding.ID Service 
                    Selamat datang di Coding.ID app, One Stop Learning App yang memberikan layanan berupa konten –
                    konten belajar programming dalam bentuk kursus online, event dan lain sebagainya.
                ' or . = 'Coding.ID Service 
                    Selamat datang di Coding.ID app, One Stop Learning App yang memberikan layanan berupa konten –
                    konten belajar programming dalam bentuk kursus online, event dan lain sebagainya.
                ')]</value>
      <webElementGuid>4f5c33fc-5063-4e55-a48a-d5e86f88dcfa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

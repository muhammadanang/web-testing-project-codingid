<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tbl_Login</name>
   <tag></tag>
   <elementGuidId>c1eb9959-84f8-4c13-9589-b3b92215cbae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonLoginTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonLoginTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d68c3e91-ee38-44b5-90f5-27ccb1f4e42b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonLoginTrack</value>
      <webElementGuid>c9377b4f-4cf1-4b23-bc1f-ee61d63d9815</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>788493a5-ee81-4c1d-8442-cf232529759c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg btn-block</value>
      <webElementGuid>2eea8d2f-8c8f-432f-b5c3-51e1c578f83a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>3aefb925-9757-4194-b424-20e200667376</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        Login
                                                    </value>
      <webElementGuid>0e868cc7-6229-4a96-9e3c-44b633f7e32d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonLoginTrack&quot;)</value>
      <webElementGuid>93faf85e-d920-47f3-ad85-375215e4ece9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonLoginTrack']</value>
      <webElementGuid>6f522db6-26de-4667-97ba-2fd967a71a85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]</value>
      <webElementGuid>10884bb7-cfe7-4542-8fef-fad04fd3ad9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[2]</value>
      <webElementGuid>11c34484-a536-4a22-93af-127e960ee17c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>beebe0ad-3b61-4418-9d62-1c01091819ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonLoginTrack' and @type = 'submit' and (text() = '
                                                        Login
                                                    ' or . = '
                                                        Login
                                                    ')]</value>
      <webElementGuid>bf025fed-045e-4c87-8d28-e89c74a0e04e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

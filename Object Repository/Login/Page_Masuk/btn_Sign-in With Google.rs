<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Sign-in With Google</name>
   <tag></tag>
   <elementGuidId>33f9fa16-c1e0-48ab-8f11-0e3c738b646e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonGoogleTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonGoogleTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a1aa77b9-ea07-4c84-8fbd-c3708d223280</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoogleTrack</value>
      <webElementGuid>f56b4456-e04f-49cf-aa4e-1c00bb40dbea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c81b5c69-4420-4ec0-a833-950184c202ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn wm-all-events btn-block bg-white</value>
      <webElementGuid>b7e24edf-0690-4ef7-995d-ac884ae373dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            
                                                            Sign-in With Google
                                                        </value>
      <webElementGuid>e7da19c3-c0df-4480-9fad-fca8736893e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoogleTrack&quot;)</value>
      <webElementGuid>24192e50-eed0-4c8c-82ce-7ab675b56899</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonGoogleTrack']</value>
      <webElementGuid>ccc19adc-c15f-4388-9aad-c51dcd5c337e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::button[1]</value>
      <webElementGuid>6cf9c19a-3c5f-43e3-816d-fd54264427bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[1]</value>
      <webElementGuid>1bce314a-ce27-47de-bab8-962ee5235126</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign-in With Google']/parent::*</value>
      <webElementGuid>1ade8700-fc46-4884-b3a5-9a610193916a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>a2b66eb7-4190-4ecb-b6b5-9f9483dfe78c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonGoogleTrack' and @type = 'button' and (text() = '
                                                            
                                                            Sign-in With Google
                                                        ' or . = '
                                                            
                                                            Sign-in With Google
                                                        ')]</value>
      <webElementGuid>b17f2877-eac2-4a53-935d-9c07c8b15229</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Change Profile/Block/Menu Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Change Profile/Dashboard Page/Edit Profile/field_Fullname'), fullname)

WebUI.setText(findTestObject('Change Profile/Dashboard Page/Edit Profile/field_Phone'), phone)

WebUI.setText(findTestObject('Change Profile/Dashboard Page/Edit Profile/field_BirthDay'), birthDay)

String image = (RunConfiguration.getProjectDir() + '/Data Files/Change Profile/') + imageName

WebUI.uploadFile(findTestObject('Change Profile/Dashboard Page/Edit Profile/field_Photo'), image)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Change Profile/Dashboard Page/Edit Profile/tbl_Save Changes'))

WebUI.verifyElementPresent(findTestObject('Change Profile/Dashboard Page/txt_Berhasil'), 3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Change Profile/Dashboard Page/tbl_OK'))

capitalName = CustomKeywords.'CustomString.capitalFirst'(fullname)

WebUI.verifyElementText(findTestObject('Change Profile/Dashboard Page/txt_Fullname'), capitalName)

WebUI.verifyElementText(findTestObject('Change Profile/Dashboard Page/txt_Phone'), phone)

WebUI.verifyElementText(findTestObject('Change Profile/Dashboard Page/txt_BirthDay'), birthDay)

WebUI.takeScreenshot()

WebUI.closeBrowser()


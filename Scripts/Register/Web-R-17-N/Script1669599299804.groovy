import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.urlWebsite)

WebUI.setViewPortSize(1200, 700)

WebUI.click(findTestObject('Register/btn_Buat Akun'))

WebUI.verifyElementPresent(findTestObject('Register/txt_Register page'), 3)

WebUI.setText(findTestObject('Register/input_name'), nama)

WebUI.setText(findTestObject('Register/input_birth_date'), tanggal_lahir)

WebUI.setText(findTestObject('Register/input_email_register'), email)

WebUI.setText(findTestObject('Register/input_whatsapp'), whatsapp)

WebUI.setText(findTestObject('Register/input_password_register'), password)

WebUI.setText(findTestObject('Register/input_password_confirmation'), konfirmasi_password)

WebUI.click(findTestObject('Register/input_inlineCheckbox1'))

WebUI.click(findTestObject('Register/btn_Daftar'))

WebUI.verifyElementPresent(findTestObject('Register/Verify Email/span_verifikasi Email'), 3)

WebUI.verifyElementPresent(findTestObject('Register/Verify Email/txt_Verifikasi email'), 3)

WebUI.verifyElementPresent(findTestObject('Register/Verify Email/form_Kirim Ulang'), 3)

WebUI.back()

WebUI.verifyElementNotPresent(findTestObject('Register/input_name'), 3)

WebUI.verifyElementNotPresent(findTestObject('Register/input_email_register'), 3)

WebUI.verifyElementNotPresent(findTestObject('Register/input_password_register'), 3)

WebUI.verifyElementNotPresent(findTestObject('Register/btn_Daftar'), 3)

WebUI.closeBrowser()
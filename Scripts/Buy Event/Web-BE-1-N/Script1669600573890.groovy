import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.urlWebsite)

WebUI.setViewPortSize(1200, 700)

WebUI.click(findTestObject('Buy Event/btn_menu_events'))

WebUI.verifyElementPresent(findTestObject('Buy Event/Events Page/div_Day 1 Close'), 3)

WebUI.verifyElementPresent(findTestObject('Buy Event/Events Page/div_Day 3 Open'), 3)

WebUI.verifyElementPresent(findTestObject('Buy Event/Events Page/div_Day 4 Open'), 3)

WebUI.click(findTestObject('Buy Event/Events Page/event_Day 3 Open'))

WebUI.verifyElementPresent(findTestObject('Buy Event/Event Detail/btn_Back to Events'), 3)

WebUI.verifyElementPresent(findTestObject('Buy Event/Event Detail/p_Harga Kelas'), 3)

WebUI.verifyElementPresent(findTestObject('Buy Event/Event Detail/btn_Beli Tiket'), 3)

WebUI.click(findTestObject('Buy Event/Event Detail/btn_Beli Tiket'))

WebUI.verifyElementPresent(findTestObject('Register/login/button_login'), 3)

WebUI.verifyElementPresent(findTestObject('Register/login/input_email'), 3)

WebUI.verifyElementPresent(findTestObject('Register/login/input_password'), 3)

WebUI.closeBrowser()
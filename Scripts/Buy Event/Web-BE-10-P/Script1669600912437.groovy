import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Buy Event/Web-BE-5-P'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Buy Event/Cart/checkbox_day 3'))

WebUI.click(findTestObject('Buy Event/Cart/btn_Checkout'))

WebUI.verifyElementPresent(findTestObject('Buy Event/Detail Pembayaran/btn_Confirm'), 3)

WebUI.verifyElementPresent(findTestObject('Buy Event/Detail Pembayaran/h3_Detail Pembayaran'), 3)

WebUI.click(findTestObject('Buy Event/Detail Pembayaran/input_payment_method'))

WebUI.click(findTestObject('Buy Event/Detail Pembayaran/btn_Confirm'))

WebUI.switchToFrame(findTestObject('Buy Event/Invoice/iframe'), 5)

WebUI.click(findTestObject('Buy Event/Invoice/div_list_bank'))

WebUI.click(findTestObject('Buy Event/Invoice/div_BCA'))

WebUI.click(findTestObject('Buy Event/Invoice/button_Back to'))

WebUI.switchToDefaultContent()

WebUI.verifyElementPresent(findTestObject('Buy Event/Invoice/h5_Invoice'), 3)

WebUI.closeBrowser()